<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot() {
    view()->composer('admin.layouts.sidebar', function ($view) {
      $sidebar = [
//        'States' => array('url' => route('admin.states.index'), 'icon' => '<i class="md md-fullscreen-exit"></i>'),
        'State Groups' => array('url' => route('admin.state-groups.index'), 'icon' => '<i class="md md-group-work"></i>'),
        'States and Questions' => array('url' => route('admin.states.index'), 'icon' => '<i class="md md-live-help"></i>'),
        'Diagnosis' => array('url' => route('admin.results.index'), 'icon' => '<i class="md md-event-note"></i>'),
        'Broadcast message' => array('url' => url('admin/bot-actions/broadcast-message'), 'icon' => '<i class="md md-send"></i>'),
      ];

      $view->with('sidebar_items', $sidebar)->with('user', \App::make('authenticator')->getLoggedUser());
    });
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register() {
    //
  }
}
