<?php

namespace App\Providers;

use App\ShortCode;
use Illuminate\Support\ServiceProvider;

class ShortCodeServiceProvider extends ServiceProvider
{
    protected $shortcodes = ['alert', 'tooltip'];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerShortCodes();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

    protected function registerShortCodes()
    {
        foreach ($this->shortcodes as $shortcode) {
            $this->app['shortcode']->register($shortcode, "App\\ShortCode@{$shortcode}");
        }
    }
}
