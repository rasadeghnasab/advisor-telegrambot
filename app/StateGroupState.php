<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaravelAcl\Http\Requests\Request;

class StateGroupState extends Model
{
  public $table = 'state_group_state';
  public $fillable = [
    'state_group_id',
    'state_id',
  ];

}
