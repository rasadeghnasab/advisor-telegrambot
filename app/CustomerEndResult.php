<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class CustomerEndResult extends Model
{
  protected $fillable = [
    'end_result_id',
    'customer_id',
  ];

  public static function endResultTags($query = '') {
    // change query to gender
    $gender = self::defineGender($query);

    $result = DB::table('customer_end_results')
      ->select(['firstname', 'lastname', DB::raw("GROUP_CONCAT(states.name SEPARATOR ', ') AS skin_type"), 'person_info.customer_id', 'age', DB::raw("REPLACE(REPLACE(person_info.gender,'1','مرد'),'0','زن') as gender"), 'person_info.province'])
      ->join('tag_end_result', 'tag_end_result.end_result_id', '=', 'customer_end_results.end_result_id')
      ->join('states', 'states.id', '=', DB::raw('REPLACE(tag_end_result.tag,"st","")'))
      ->join('person_info', 'person_info.customer_id', '=', 'customer_end_results.customer_id')
      ->where('tag_end_result.tag', 'like', 'st%')
      ->where('customer_end_results.closed', 1)
      ->where('person_info.closed', 1)
      ->orWhere('states.name', 'like', "%$query%")
      ->orWhere('person_info.age', 'like', "%$query%")
      ->orWhere('person_info.province', 'like', "%$query%")
      ->groupBy('person_info.id')
      ;

    if($gender !== false) $result->orWhere('person_info.gender', $gender);

//    if(!$customer_ids->isEmpty()) {
//      $result->whereIn('customer_end_results.customer_id', $customer_ids);
//    }

    return $result->get();
  }

  private static function defineGender($query) {
    $gender = false;

    $man = ['man', 'مرد', 'مذکر'];
    $woman = ['woman', 'زن', 'مونث'];
    if(in_array($query, $man)) {
      $gender = 1;
    }
    elseif(in_array($query, $woman)) {
      $gender = 0;
    }

    return $gender;
  }

  public function tags() {
    return $this->belongsToMany('App\TagEndResult');
  }
}
