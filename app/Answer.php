<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Phoenix\EloquentMeta\MetaTrait;


class Answer extends Model
{
  use MetaTrait;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'body',
    'next_state_id',
    'question_id', // remove in the future
    'weight'
  ];

  public function question() {
    return $this->belongsTo('App\Question');
  }
  
  public function next_state() {
    return $this->hasOne('App\State', 'id', 'next_state_id');
  }
  
  public function scopeWeightOrdered($query) {
    $query->orderBy('weight', 'asc')->orderBy('created_at', 'desc');
  }

  public function scopeValidateAnswers($query, $message) {
    $query->where('body', $message);
  }
  public function answeredQuestion() {
    return $this->hasMany('App\Answer');
  }
}
