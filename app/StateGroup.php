<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateGroup extends Model {
  public $fillable = [
    'name',
  ];
  
  public function states() {
    return $this->belongsToMany('App\State', 'state_group_state');
  }
}
