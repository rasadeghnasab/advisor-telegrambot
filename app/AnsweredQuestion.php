<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnsweredQuestion extends Model
{
    //
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'answered_questions';

  protected $fillable = [
    'question_id',
    'answer_id',
    'customer_id',
    'closed',
  ];


  public function answer() {
    return $this->belongsTo('App\Answer');
  }
}
