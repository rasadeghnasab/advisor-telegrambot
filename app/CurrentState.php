<?php

namespace App;

use App\State;
use App\Events\TestComplete;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;

class CurrentState extends Model
{

  protected $table = 'current_state';

  protected $fillable = [
    'last_state',
    'current_state',
    'current_sub_state',
    'customer_id',
  ];

  public $next_state;
  public $next;
  private $message;

  public static function findCurrentState($customer_id)
  {
    return self::firstOrNew(['customer_id' => $customer_id]);
  }

  public function customer()
  {
    $this->hasOne('App\customer');
  }

  /**
   * @param $message
   *
   * @return bool
   */
  public function isValidAnswer($message)
  {
    // TODO::answers must be limited by features.
    $allowed = [
      'back',
      'بازگشت',
      '/start',
    ];

    if ($this->current_state == NULL || in_array(strtolower($message), $allowed)) {
      return TRUE;
    }
    elseif ($this->current_sub_state) {
      $state    = State::find($this->current_state);
      $question = $state->questions()->find($this->current_sub_state);
      $answer   = $question->answers()->ValidateAnswers($message)->first();

      return $answer ? TRUE : FALSE;
    }

    return FALSE;
  }

  public function searchable()
  {
    return DB::table('states')
     ->whereIn('id', explode('>', $this->last_state . '>' . $this->current_state))
     ->where('deletable', 1)
     ->exists();
  }

  public function restart()
  {
    // remove all none closed records

    return $this->mainMenu();
  }

  public function next()
  {
    $state = NULL;
    // If user has no record in current state table.
    if (!isset($this->current_state) || $this->current_state == 'null') {
      // show main menu
      $current_state = $this->mainMenu();
    }
    elseif ($this->current_sub_state && !Question::find($this->current_sub_state)->save_answer) {
      // if the question has no flag for answer save.
      $next_state_id = Question::find($this->current_sub_state)
                               ->answers()
                               ->where('body', '=', $this->message)
                               ->first()->next_state_id
      ;

      $current_state = new self([
        'last_state'        => $this->lastStateAdd(),
        'current_state'     => $next_state_id,
        'current_sub_state' => State::find($next_state_id)
                                    ->firstQuestion() == NULL ? NULL : State::find($next_state_id)
                                                                            ->firstQuestion()->id,
      ]);
    }
    elseif ($this->current_sub_state) {
      $state         = State::find($this->current_state);
      $next_question = $this->nextQuestion();
      if (!$next_question) { // That means we rich to the end.
        // we must find out which state we must to go. :) base on answers submissions.
        $next_state = $state->nextState($this);

        $current_state = new self([
          'last_state'        => $this->lastStateAdd(),
          'current_state'     => $next_state->id,
          'current_sub_state' => isset($next_state->firstQuestion()->id) ? $next_state->firstQuestion()->id : NULL,
        ]);
      }
      else {
        $this->setAttribute('current_sub_state', $next_question->id);
        $current_state = $this;
      }
    }

    $current_state->customer_id = $this->customer_id;

    return $current_state;
  }

  /**
   * This method must be review and edited in the feature.
   *
   * @return \App\CurrentState
   */
  public function back()
  {
    $state = NULL;
    if (!$this->last_state) {
      // Go to main menu.
      $current_state = $this->mainMenu();
    }
    elseif ($this->current_sub_state) {
      $state            = State::find($this->current_state);
      $current_question = Question::find($this->current_sub_state);
      $prev_question    = $state->questions()
                                ->prevQuestion($current_question->weight)
                                ->first()
      ;

      if (!$prev_question) { // That means we rich to the end.
        $last_state = collect(explode('>', $this->last_state));
//
//        // we must find out which state we must to go. :) base on answers submissions.
        $prev_state = $state->prevState($last_state->pop());

        $current_state = new self([
          'last_state'        => $last_state->implode('>'),
          'current_state'     => $prev_state->id,
          'current_sub_state' => $prev_state->lastQuestion()->id,
        ]);

        if($state->result_type == 'calc_result') {
          // we must delete calculated result when we go backward.
          // remove last inserted result for this user.
          Result::where('customer_id', $this->customer_id)->where('closed', 0)->orderBy('created_at', 'desc')->first()->delete();
        }
      }
      elseif($prev_question) {
        $this->setAttribute('current_sub_state', $prev_question->id);
        $current_state = $this;
        // return this state; $state;
      }
    }
    else {
      // go to last state, latest question
      $state            = State::find($this->current_state);
      $last_state = collect(explode('>', $this->last_state));

      // we must find out which state we must to go. :) base on answers submissions.
      $prev_state = $state->prevState($last_state->pop());

      $current_state = new self([
       'last_state'        => $last_state->implode('>'),
       'current_state'     => $prev_state->id,
       'current_sub_state' => $prev_state->lastQuestion()->id,
      ]);
    }

    // remove current_sub_State from answered_questions table
    AnsweredQuestion::where('question_id', $current_state->current_sub_state)
                    ->where('customer_id', $this->customer_id)
                    ->where('closed', 0)
                    ->delete()
    ;

    $current_state->customer_id = $this->customer_id;

    return $current_state;
  }

  public function router($routeCommand)
  {
    $this->message = $routeCommand;
    $routeCommand  = strtolower($routeCommand);
    if ($routeCommand == 'back') {
      // back
      return $this->back();
    }
    elseif ($routeCommand == '/start' || $routeCommand == '/restart') {
      return $this->restart();
    }
    else {
      // next
      return $this->next();
    }
  }

  public function scopeCurrent($customer_id, $query)
  {
    return $query->where('customer_id', $customer_id);
  }

  private function mainMenu()
  {
    $state         = State::where('name', 'main-menu')->first();
    $current_state = new self([
      'last_state'        => NULL,
      'current_state'     => $state->id,
      'current_sub_state' => $state->firstQuestion()->id,
      'customer_id' =>  $this->customer_id,
    ]);

    Event::fire(new TestComplete($current_state));

    return $current_state;
  }

  private function nextQuestion()
  {
    $current_question = Question::find($this->current_sub_state);
    $state            = State::find($this->current_state);

    return $state->questions()
                 ->nextQuestion($current_question->weight)
                 ->first()
      ;
  }

  public function state()
  {
    return $this->belongsTo('App\State', 'current_state');
  }

  private function lastStateAdd()
  {
    return collect(explode('>', $this->last_state))
      ->merge($this->current_state)
      ->unique()
      ->implode('>')
      ;
  }

  public static function updateAfterChangeState($result, $state_change)
  {
    if ($result->response && $result->save && $state_change->current_state->id) {
      self::where('id', $state_change->current_state->id)
                  ->update($state_change->new_state->attributesToArray())
      ;

      return 'updated';
    }
    elseif ($result->response && $result->save) {
      $state_change->new_state->save();

      return 'new user saved';
    }
    else {
      return 'perform no action';
    }
  }

}