<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndResult extends Model {

  protected $fillable = [
    'body',
  ];

  public function tags() {
    return $this->hasMany('App\TagEndResult', 'end_result_id');
  }
  
  public function products() {
    return $this->hasMany('App\RelatedProduct', 'end_result_id');
  }
}
