<?php

namespace App;

use App\AnsweredQuestion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class State extends Model
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
   'name',
   'result_type',
   'next_state',
   'deletable',
  ];

  public function gender()
  {
    return $this->result_type == 'gender';
  }

  public function age()
  {
    return $this->result_type == 'age';
  }

  public function province()
  {
    return $this->result_type == 'province';
  }

  public function nextState($current_state, $not_ignore_next_state = true)
  {
    $state = State::find($current_state->current_state);
    if ($state->next_state && $not_ignore_next_state) {
      return self::find($state->next_state);
    };

    $answered_questions = AnsweredQuestion::with('answer')
     ->where('customer_id', $current_state->customer_id)
     ->whereIn('question_id', $this->questions->lists('id'))
     ->where('closed', 0)
     ->get();

    $next_state_ids = [];
    foreach ($answered_questions as $answered_question) {
      $next_state_ids[] = $answered_question->answer->next_state_id;
    }

    $counted_values = array_count_values($next_state_ids);
    $next_state_id = array_search(max($counted_values), $counted_values);

    return self::find($next_state_id);
  }

  public function prevState($state_id)
  {
    return self::find($state_id);
  }

  public function saveAnswer($answer_text, $question_id, $customer_id)
  {
//    $state    = self::find($this->current_state);
    $question = $this->questions()->find($question_id);
    if (!$question || !$question->save_answer) return;
    $answer = $question->answers()->ValidateAnswers($answer_text)->first();

    if(!$answer) {
      return;
    }

    AnsweredQuestion::create([
     'question_id' => $question_id,
     'answer_id'   => $answer->id,
     'customer_id' => $customer_id,
     'closed'      => 0,
    ])->save();
  }

  public function firstQuestion()
  {
    return $this->questions()->orderBy('weight')->first();
  }

  public function scopeLastQuestion()
  {
    return $this->questions()->orderBy('weight', 'desc')->first();
  }

  /**
   * A State can have many questions
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function questions()
  {
    return $this->hasMany('App\Question');
  }

  public function stateGroups()
  {
    return $this->belongsToMany('App\StateGroup', 'state_group_state');
  }
}
