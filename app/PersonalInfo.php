<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalInfo extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'person_info';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
   'firstname',
   'lastname',
   'customer_id',
   'gender',
   'age',
   'province',
  ];

  /**
   * @param $user
   * @param $current_state
   * @param $message_text
   */
  public static function personalInfoManagement($user, $current_state, $message_text)
  {
    if ($current_state->state != null && ($current_state->state->gender() || $current_state->state->age() || $current_state->state->province())) {
      $state_type = $current_state->state->result_type;
      $info = ['customer_id' => $user->getId(), 'firstname' => htmlentities($user->getFirstName()), 'lastname' => htmlentities($user->getLastName()), 'closed' => 0];
      PersonalInfo::updateOrCreate($info, [$state_type => $message_text]);
    }
  }

  /*
  |--------------------------------------------------------------------------
  | Scope
  |--------------------------------------------------------------------------
  */
  /**
   * @param $query
   * @param $customer_id
   */
  public function scopeNewUser($query, $customer_id)
  {
    $query->where('customer_id', $customer_id);
  }

  /**
   * @param $query
   * @param $customer_id
   */
  public function scopeCustomerPersonalInfo($query, $customer_id)
  {
    $query->where('customer_id', $customer_id)->where('closed', 0);
  }

  /*
  |--------------------------------------------------------------------------
  | Accessors
  |--------------------------------------------------------------------------
  */

  /**
   * @return mixed
   */
  public function getGender()
  {
    return $this->attributes['gender'];
  }

  /*
  |--------------------------------------------------------------------------
  | Mutators
  |--------------------------------------------------------------------------
  */

  /**
   * @param $gender
   */
  public function setGenderAttribute($gender)
  {
    switch ($gender) {
      case 'men':
      case 'man':
      case 'مرد':
      case 'مذکر':
        $gender = 1;
        break;
      case 'women':
      case 'woman':
      case 'زن':
      case 'مونث':
        $gender = 0;
        break;
      default:
        $gender = 1;
    }

    $this->attributes['gender'] = $gender;
  }

  /**
   * @param $gender
   * @return string
   */
  public function getGenderAttribute($gender)
  {
    switch ($gender) {
      case 1:
        $gender = 'مرد';
        break;
      case 0:
        $gender = 'زن';
        break;
      default:
        $gender = 'مرد';
    }

    return $gender;
  }
}
