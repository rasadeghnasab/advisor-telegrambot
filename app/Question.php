<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {
  protected $fillable = [
    'body',
    'state_id',
    'weight',
    'searchable',
    'save_answer',
  ];
  private static $searchableQueryMinLength = 4;

  public static function search($searchQuery)
  {
    if(! self::validateSearchableQuery($searchQuery)) {
      return false;
    }

    // search in searchable questions in searchable states and return results.
    return \DB::table('states')
     ->join('questions', 'states.id', '=', 'questions.id')
     ->where('states.deletable', '=', 1)
     ->where('questions.searchable', '=', 1)
     ->where('questions.body', 'LIKE', "%{$searchQuery}%")
     ->pluck('body');
     ;
  }

  private static function validateSearchableQuery($searchableQuery)
  {
    return strlen($searchableQuery) >= self::$searchableQueryMinLength;
  }

  public function scopeNextQuestion($query, $weight) {
    return $query->where('weight', '>', $weight)->orderBy('weight')->take(1);
  }

  public function scopePrevQuestion($query, $weight) {
    return $query->where('weight', '<', $weight)->orderBy('weight', 'desc')->take(1);
  }

  public function state() {
    return $this->belongsTo('App\State');
  }

  public function answers() {
    return $this->hasMany('App\Answer');
  }

  /**
   * A question can have many questions
   *
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function answeredQuestions() {
    return $this->hasMany('App\AnsweredQuestion');
  }

  public function scopeWeightOrder($query) {
    return $query->orderBy('weight');
  }
}
