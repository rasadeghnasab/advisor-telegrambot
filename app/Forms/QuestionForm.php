<?php

namespace App\Forms;

use App\State;
use Kris\LaravelFormBuilder\Form;

class QuestionForm extends Form {
  public function buildForm() {
    $this
      ->add('body', 'textarea', [
        'rules' => 'required|min:5|max:2000',
        'label' => 'متن سوال',
      ])
      ->add('state_id', 'select', [
        'choices'     => State::lists('name', 'id')->toArray(),
        'empty_value' => '= انتخاب مرحله =',
        'attr' => ['class' => 'select-select2 form-control'],
        'label' => 'این سوال مربوط به کدام مرحله است؟',
        'rules' => 'required',
      ])
      ->add('searchable', 'checkbox', ['label' => 'در جستجوها نمایش داده شود'])
      ->add('save_answer', 'checkbox', ['label' => 'ذخیره پاسخ ها'])
      ->add('submit', 'submit', [
        'attr'  => ['class' => 'btn btn-primary'],
        'label' => 'ثبت',
      ])
    ;
  }
}
