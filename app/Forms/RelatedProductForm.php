<?php

namespace App\Forms;

use App\EndResult;
use App\State;
use Kris\LaravelFormBuilder\Form;

class RelatedProductForm extends Form {
  public function buildForm() {
    $this
      ->add('description', 'textarea', [
      'rules' => 'required|min:20',
      'label' => 'توضیحات محصول',
      ])
//      ->add('end_result_id', 'select', [
//        'choices'     => EndResult::pluck('body', 'id')->map(function ($item) {
//          return str_limit($item, '20');
//        })->toArray(),
//        'empty_value' => 'انتخاب تشخیص ',
//        'label'       => 'محصول مرتبط به تشخیص ...',
//        'rules'       => 'required',
//        'attr'        => [],
//      ])
      ->add('submit', 'submit', [
        'attr'  => ['class' => 'btn btn-primary'],
        'label' => 'ثبت',
      ])
    ;
  }
}
