<?php

namespace App\Forms;

use App\State;
use Kris\LaravelFormBuilder\Form;

class StateForm extends Form
{
  public function buildForm()
  {
    $this->add('name', 'text', ['rules' => 'required|min:2', 'label' => 'نام'])
     ->add('result_type', 'select', [
      'rules'       => 'required',
      'label'       => 'نوع شروع',
      'choices'     => [
       'nothing'          => 'هیچکدام',
       'reset_last_state' => 'حذف مراحل قبلی',
       'calc_result'      => 'محاسبه نتیجه',
       'calc_points_result'      => 'محاسبه نتیجه امتیازات',
       'show_end_result'  => 'نمایش نتیجه',
       'offer_product'    => 'پیشنهاد محصول',
       'gender'           => 'جنسیت',
       'age'              => 'سن',
       'province'         => 'استان',
      ],
      'empty_value' => '=== انتخاب نوع شروع ==='
     ])
     ->add('next_state', 'select', [
      'choices' => State::all()->pluck('name', 'id')->toArray(),
      'attr' => ['class' => 'select-select2 form-control'],
      'empty_value' => '=== در صورت خالی گذاشتن این فیلد به صورت اتوماتیک محاسبه میشود ===',
      'label' => 'مرحله بعدی',
     ])
     ->add('deletable', 'checkbox', ['label' => 'قابل جستجو',])
     ->add('submit', 'submit', [
      'attr'  => ['class' => 'btn btn-primary'],
      'label' => 'ثبت',
     ]);
  }
}
