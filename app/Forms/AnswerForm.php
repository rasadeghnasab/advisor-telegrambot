<?php

namespace App\Forms;

use App\State;
use Kris\LaravelFormBuilder\Form;

class AnswerForm extends Form {
  public function buildForm() {
    $this
     ->add('body', 'text', [
      'rules' => 'required',
      'label' => 'متن پاسخ',
    ])
     ->add('next_state_id', 'select', [
      'choices'     => State::lists('name', 'id')->prepend('امتیاز', 0)->toArray(),
//      'empty_value' => '=== یک حالت را انتخاب نمایید ===',
      'label'       => 'مرحله بعدی',
      'rules'       => 'required',
      'attr' => [
        'class' => 'select-select2 form-control',
      ],
    ])
     ->add('gender', 'select', [
      'choices' => State::firstOrNew(['result_type' => 'gender'])->questions()->first()->answers()->pluck('body', 'body')->toArray(),
//      'choices'     => ['زن', 'مرد'],
      'empty_value' => '= پاسخ فقط برای این جنسیت نمایش داده شود =',
      'label'       => 'جنسیت',
      'rules'       => '',
      'attr' => [
//        'class' => 'select-select2 form-control',
      ],
    ])
     ->add('age', 'select', [
      'choices' => State::firstOrNew(['result_type' => 'age'])->questions()->first()->answers()->pluck('body', 'id')->toArray(),
//      'empty_value' => '= پاسخ برای سنین زیر وجود داشته باشد =',
      'label'       => 'سن',
      'rules'       => '',
      'attr' => [
        'multiple' => 'multiple',
        'class' => 'select-select2 form-control'
      ],
    ])
     ->add('province', 'select', [
      'choices' => State::firstOrNew(['result_type' => 'province'])->questions()->first()->answers()->pluck('body', 'id')->toArray(),
//      'empty_value' => '= پاسخ برای سنین زیر وجود داشته باشد =',
      'label'       => 'استان',
      'rules'       => '',
      'attr' => [
        'multiple' => 'multiple',
        'class' => 'select-select2 form-control'
      ],
    ])
     ->add('point', 'text', [
      'label' => 'امتیاز',
      'attr' => [
        'placeholder' => 'در صورت خالی ماندن امتیازی به این جواب تعلق نمیگیرد'
      ],
     ])

     ->add('submit', 'submit', [
      'attr'  => ['class' => 'btn btn-primary'],
      'label' => 'ثبت',
    ])
     ->add('ثبت و جدید', 'submit', [
      'attr'  => ['class' => 'btn btn-success', 'name' => 'insert_and_new',
                  'value' => 'insert_and_new'],
    ])
    ;
  }
}
