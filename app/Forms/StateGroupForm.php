<?php

namespace App\Forms;

use App\State;
use Kris\LaravelFormBuilder\Form;

class StateGroupForm extends Form {
  public function buildForm() {
    $this
      ->add('name', 'text', [
        'rules' => 'required|min:5',
        'label' => 'نام گروه',
      ])
      ->add('submit', 'submit', [
        'attr'  => ['class' => 'btn btn-primary'],
        'label' => 'ثبت',
      ])
    ;
  }
}
