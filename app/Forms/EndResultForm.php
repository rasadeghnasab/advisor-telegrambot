<?php

namespace App\Forms;

use App\State;
use Kris\LaravelFormBuilder\Form;

class EndResultForm extends Form
{
    public function buildForm()
    {
        $this
          ->add('gender', 'select', [
            'label'   => 'جنسیت',
            'choices' => [
              'زن',
              'مرد'
            ],
            'empty_value' => 'جنسیت را مشخص نمایید'
          ])
          ->add('age', 'select', [
            'attr' => [
              'class' => 'select-select2 form-control',
            ],
            'rules'   => 'required',
            'label'   => 'سن',
            'choices' => State::firstOrNew(['result_type' => 'age'])->questions()->first()->answers()->pluck('body', 'id')->toArray(),
            'empty_value' => 'سن مورد نظر را انتخاب نمایید'
          ])
          ->add('tags', 'select', [
            'attr' => [
              'multiple' => 'multiple',
              'class' => 'select-select2 form-control'
            ],
            'label'   => 'ویژگی ها',
            'choices' => State::where('result_type', 'calc_result')->pluck('name', 'id')->toArray(),
//            'empty_value' => 'ویژگی ها',
          ])
          ->add('body', 'textarea', [
            'rules' => 'required|min:5',
            'label' => 'تشخیص شما',
          ])
          ->add('submit', 'submit', [
            'attr'  => ['class' => 'btn btn-primary'],
            'label' => 'ثبت',
          ])
        ;
    }
}
