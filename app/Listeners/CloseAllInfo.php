<?php

namespace App\Listeners;

use App\BotApi;
use App\Events\TestComplete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class CloseAllInfo
{

  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  TestComplete $event
   *
   * @return void
   */
  public function handle(TestComplete $event)
  {
    // Close all prev states. after show products to user.
    BotApi::closeUserActivities($event->current_state->customer_id);        //
  }
}
