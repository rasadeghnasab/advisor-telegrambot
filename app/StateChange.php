<?php
namespace App;

use App\Events\TestComplete;
use Illuminate\Support\Facades\Event;
use App\State;
use App\BotApi;
use Illuminate\Support\Facades\Log;

class StateChange {
  public $current_state;
  public $new_state;

  public function __construct($current_state, $new_state) {
    $this->current_state = $current_state;
    $this->new_state = $new_state;
  }

  public function stateController($movement = 'next') {
    if(($this->new_state->current_state != $this->current_state->current_state && $movement != 'back') ||
       $this->current_state->current_state == null) {
      // This means that we go one state forward. and we must do something base on state requirements.
      $enterValue = $this->enterState();
      $this->exitState();

      return $enterValue;
    }
    elseif($state = State::find($this->new_state->current_state)) {
      return isset($state->result_type) ? $state->result_type : 'nothing';
    }
  }

  private function enterState() {
    // do something base on state that we're on.
    // if it is normal state
    // Right side of equation must be fetch from another table.
    $state = State::find($this->new_state->current_state);
    $result_type = isset($state->result_type) ? $state->result_type : 'nothing';

    if($result_type == 'calc_result') {
      // calculate the result for this state and save it to the database.
      // user ..., tags (tags are the states (use state_id)),
      // remove last result for user ?!?!?!
      // results_tags
      $state = State::find($this->current_state->current_state);
      $result = $next_state = $state->nextState($this->current_state, false)->id;

      // Save last result
      Result::create(['customer_id' => $this->current_state->customer_id, 'result' => $result])->save();
    }
    if($result_type == 'calc_points_result') {

     // calculate points
//      Log::info(State::find($this->current_state->current_state)->questions->pluck('id'));
//      Log::warning($this->current_state);
//      $qids = State::find($this->current_state->current_state)->questions->pluck('id');

      $answeredQuestions = AnsweredQuestion::where('customer_id', $this->current_state->customer_id)->where('closed', 0)->pluck('answer_id');

      $answers = Answer::whereIn('id', $answeredQuestions)->get();

      $result = 0;
      foreach ($answers as $answer) {
//        Log::info($answer);
        $result += $answer->getMeta('point');
      }

//      Log::warning($answers);
//      Log::info($result);

      // Save last result
      Result::create(['customer_id' => $this->current_state->customer_id, 'result' => $result])->save();
    }
    elseif($result_type == 'show_end_result' && false) {
      //
    }
    elseif ($result_type == 'offer_product' && false) {
      //
    }
    elseif ($result_type == 'reset_last_state') {
      $this->new_state->last_state = null;

//      Event::fire(new TestComplete($this->new_state));
    }

    return $result_type;
  }

  private function exitState() {
    return;
    // do something base on state that we're on.
    // if it is normal state
    // Right side of equation must be fetch from another table.
    $state = State::find($this->current_state->current_state);
    $result_type = isset($state->result_type) ? $state->result_type : 'nothing';

    if($result_type == 'calc_result') {
      // calculate the result for this state and save it to the database.
      // user ..., tags (tags are the states (use state_id)),
      // remove last result for user ?!?!?!
      // results_tags
      $state = State::find($this->current_state->current_state);
      $result = $next_state = $state->nextState($this->current_state, false)->id;

      // Save last result
      Result::create(['customer_id' => $this->current_state->customer_id, 'result' => $result])->save();
    }
    elseif($result_type == 'show_end_result' && false) {
      //
    }
    elseif ($result_type == 'offer_product' && false) {
      //
    }
    elseif ($result_type == 'reset_last_state') {
      $this->new_state->last_state = null;
    }

    return $result_type;
  }
}