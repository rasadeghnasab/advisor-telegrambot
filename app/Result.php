<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
      'customer_id',
      'result',
      'closed',
    ];

    public function scopeGetResults($query, $customer_id) {
        $query->where('customer_id', $customer_id)->where('closed', 0);
    }
}
