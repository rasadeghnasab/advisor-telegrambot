<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TestComplete extends Event
{
    use SerializesModels;
    public $current_state;

    /**
     * Create a new event instance.
     *
     * @param $current_state
     */
    public function __construct($current_state)
    {
        $this->current_state = $current_state;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
