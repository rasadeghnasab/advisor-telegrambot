<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagEndResult extends Model
{
    //
  protected $table = 'tag_end_result';

  protected $fillable = [
    'end_result_id',
    'tag',
  ];

  public $timestamps = true;

  public function scopeDiagnosis($query, $tags) {
    $query
      ->select('end_result_id')
      ->whereIn('tag', $tags)
      ->groupBy('end_result_id')
      ->havingRaw("COUNT(*) = " . count($tags))
    ;
  }

  public function endResult() {
    return $this->belongsTo('App\EndResult');
  }

}
