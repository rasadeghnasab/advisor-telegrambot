<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model {
  public $fillable = [
    'entity_id',
    'bundle',
    'type',
    'value',
  ];

  public function scopeQuestionFilter($query, $answer_ids) {
    $query->where('bundle', 'answer')->whereIn('entity_id', $answer_ids);
  }

  public function scopeAnswerFilter($query, $answer_ids) {
    $query->where('bundle', 'answer')->whereIn('entity_id', $answer_ids);
  }
}
