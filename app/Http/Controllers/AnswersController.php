<?php

namespace App\Http\Controllers;

use App\PersonalInfo;
use DB;
use App\Answer;
use App\Feature;
use App\Question;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder as KFB;

use App\Http\Requests;

class AnswersController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @param \App\Question $question
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Question $question) {
    // list all answers
    $answers = $question->answers()->WeightOrdered()->get();

    return view('admin.answers.list', compact('question', 'answers'));
  }

  public function weightOrder(Request $request) {
    $weights = $request->get('weight');

    foreach ($request->get('aid') as $index => $aid) {
      Answer::where('id', $aid)->update(['weight' => $weights[$index]]);
    }

    return redirect()->back();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   * @param \App\Question                        $question
   *
   * @return \Illuminate\Http\Response
   */
  public function create(KFB $formBuilder, Question $question) {
    //
    $form = $formBuilder->create('App\Forms\AnswerForm', [
      'method' => 'POST',
      'url'    => route('admin.questions.answers.store', $question),
    ]);

    return view('admin.answers.create', compact('form', 'question'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @param \App\Question             $question
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, Question $question) {
    $answer = new Answer($request->all());

    $question->answers()->save($answer);

    // gender
    $features = [];
    if ($gender = $request->get('gender')) {
      $features[] = ['type' => 'gender', 'value' => $gender];
    };
    // age
    if ($age = $request->get('age')) {
      $value = is_array($age) ? implode(', ', $age) : $age;
      $features[] = ['type' => 'age', 'value' => $value];
    };
    // province
    if ($province = $request->get('province')) {
      $value = is_array($province) ? implode(', ', $province) : $province;
      $features[] = ['type' => 'province', 'value' => $value];
    };

    $base_feature = ['entity_id' => $answer->id, 'bundle' => 'answer'];
    foreach ($features as $feature) {
      Feature::create(array_merge($base_feature, $feature));
    }

    if($request->has('point')) {
      $answer->addMeta('point', $request->get('point'));
    }

    if($request->has('insert_and_new')) {
      return redirect()->back();
    }
    
    return redirect()->route('admin.questions.answers.index', $question);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Question $question
   * @param \App\Answer $answer
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   *
   */
  public function edit(Question $question, Answer $answer, KFB $formBuilder) {
    $form     = $formBuilder->create('App\Forms\AnswerForm', [
      'method' => 'PATCH',
      'url'    => route('admin.questions.answers.update', [$question, $answer]),
      'model'  => $answer,
    ]);

    $form->modify('submit', 'submit', [
      'label' => 'ویرایش',
    ]);

    $form->modify('point', 'text', [
      'default_value' => $answer->getMeta('point'),
    ]);

    $ages = Feature::where([
      'entity_id' => $answer->id,
      'bundle'    => 'answer',
      'type'      => 'age',
    ])->first();

    if ($ages) {
      $form->modify('age', 'select', ['selected' => explode(', ', $ages->value)]);
    }

    $gender = Feature::where([
      'entity_id' => $answer->id,
      'bundle'    => 'answer',
      'type'      => 'gender',
    ])->first();

    if ($gender) {
      $form->modify('gender', 'select', ['selected' => $gender->value]);
    }

    $provinces = Feature::where([
      'entity_id' => $answer->id,
      'bundle'    => 'answer',
      'type'      => 'province',
    ])->first();
    if ($provinces) {
      $form->modify('province', 'select', ['selected' => explode(', ', $provinces->value)]);
    }

    return view('admin.answers.create', compact('form', 'question'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param \App\Question             $question
   * @param \App\Answer               $answer
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   *
   */
  public function update(Request $request, Question $question, Answer $answer) {
    $answer->update($request->all());

    if($request->has('point')) {
      $answer->updateMeta('point', $request->get('point'));
    }
    else {
      $answer->deleteMeta('point');
    }


    Feature::where('entity_id', $answer->id)->where('bundle', 'answer')->delete();
    // gender
    $features = [];
    if ($gender = $request->get('gender')) {
      $features[] = ['type' => 'gender', 'value' => $gender];
    };

    // age
    if ($age = $request->get('age')) {
      // TODO:: this question answer, is messy :| fix it later.
      $value = is_array($age) ? implode(', ', $age) : $age;
      $features[] = ['type' => 'age', 'value' => $value];
    };

    // province
    if ($province = $request->get('province')) {
      $value = is_array($province) ? implode(', ', $province) : $province;
      $features[] = ['type' => 'province', 'value' => $value];
    };

    $base_feature = ['entity_id' => $answer->id, 'bundle' => 'answer'];
    foreach ($features as $feature) {
      Feature::create(array_merge($base_feature, $feature));
    }

    return redirect()->route('admin.questions.answers.index', [
      $question,
      $answer,
    ]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Question $question
   * @param \App\Answer   $answer
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function destroy(Question $question, Answer $answer) {
    DB::beginTransaction();

    try {
      $answer->delete();

      DB::commit();
      return 'true';
    } catch (\Exception $e) {
      DB::rollback();
      // something went wrong
      return 'false';
    }

  }
}
