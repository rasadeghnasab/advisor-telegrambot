<?php

namespace App\Http\Controllers;

use App\EndResult;
use App\RelatedProduct;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder as KFB;

use App\Http\Requests;

class RelatedProductsController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @param \App\EndResult $result
   *
   * @return \Illuminate\Http\Response
   */
  public function index(EndResult $result) {
    return view('admin.products.list', compact('result'));
  }

  public function weightOrder(Request $request) {
    $weights = $request->get('weight');

    foreach ($request->get('aid') as $index => $aid) {
      RelatedProduct::where('id', $aid)->update(['weight' => $weights[$index]]);
    }

    return redirect()->back();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   * @param \App\EndResult                       $result
   *
   * @return \Illuminate\Http\Response
   */
  public function create(KFB $formBuilder, EndResult $result) {
    //
    $form = $formBuilder->create('App\Forms\RelatedProductForm', [
      'method' => 'POST',
      'url'    => route('admin.results.products.store', $result),
    ]);

    return view('admin.products.create', compact('form'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @param \App\EndResult            $result
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, EndResult $result) {
    $product = new RelatedProduct($request->all());

    $result->products()->save($product);

    return redirect()->route('admin.results.products.index', $result);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   *
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\EndResult                       $results
   *
   * @param \App\RelatedProduct                  $products
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   * @internal param \App\RelatedProduct $product
   *
   * @internal param int $id
   */
  public function edit(EndResult $results, RelatedProduct $products, KFB $formBuilder) {
    //
    $form = $formBuilder->create('App\Forms\RelatedProductForm', [
      'method' => 'PUT',
      'url'    => route('admin.results.products.update', [$results, $products]),
      'model'  => $products,
    ]);

    $form->modify('submit', 'submit', [
      'label' => 'ویرایش',
    ]);

    return view('admin.products.create', compact('form'));
//    return RelatedProduct::destroy($product->id);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param \App\EndResult            $results
   * @param \App\RelatedProduct       $products
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function update(Request $request, EndResult $results, RelatedProduct $products) {
    $products->update($request->all());

    return redirect()->route('admin.results.products.index', [$results, $products]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\EndResult      $result
   * @param \App\RelatedProduct $product
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function destroy(EndResult $result, RelatedProduct $product) {
    return RelatedProduct::destroy($product->id);
  }
}
