<?php

namespace App\Http\Controllers;

use App\Answer;
use App\CustomerEndResult;
use App\EndResult;
use App\Question;
use App\RelatedProduct;
use App\State;
use App\TagEndResult;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Kris\LaravelFormBuilder\FormBuilder as KFB;

class EndResultController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $answers = State::where('result_type', 'age')->first()->questions()->first()->answers()->orderBy('body')->get();

    return view('admin.end_results.group-list', compact('answers'));
  }

  public function resultsOnAge($age) {
    $age = Answer::where('body', $age)->first()->id;
    $endResultIds = TagEndResult::where('tag', 'pa'. $age)->pluck('end_result_id');
    $results = EndResult::whereIn('id', $endResultIds)->get();

    return view('admin.end_results.list', compact('results'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   */
  public function create(KFB $formBuilder) {
    //
    $form = $formBuilder->create('App\Forms\EndResultForm', [
      'method' => 'POST',
      'url'    => route('admin.results.store'),
    ]);

    return view('admin.end_results.create', compact('form'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request|Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    // pg personal gender'
    if($request->get('gender')) {
      $gender = 'pg' . $request->get('gender');
    }

    if($request->get('age')) {
      // pa personal age
      $age = 'pa' . $request->get('age');
    }

    $end_result_message = EndResult::create($request->all());

    $states = (array) $request->get('tags');

    $new_states = [];
    foreach ($states as $index => $state) {
      $new_states[] = [
        'end_result_id' => $end_result_message->id,
        // st state
        'tag'           => 'st' . $state,
        'created_at'    => Carbon::now(),
        'updated_at'    => Carbon::now(),
      ];
    }

    $age_gender = [];
    $time = Carbon::now();

    if(isset($gender))
      $age_gender[] = [
        'end_result_id' => $end_result_message->id,
        'tag'           => $gender,
        'created_at'    => $time,
        'updated_at'    => $time,
      ];
    if(isset($age))
      $age_gender[] = [
        'end_result_id' => $end_result_message->id,
        'tag'           => $age,
        'created_at'    => $time,
        'updated_at'    => $time,
      ];

    $tag_end_result_values = array_merge($age_gender, $new_states);

    TagEndResult::insert($tag_end_result_values);

    return redirect()->back();
//    return redirect()->route('admin.results.products.index', $end_result_message);
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Question $question
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function show(Question $question) {
    return $question->state->name;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\EndResult                       $endResult
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   * @internal param \App\Question $question
   * @internal param int $id
   */
  public function edit(EndResult $endResult, KFB $formBuilder) {
//    dd($endResult->tags);
    $form = $formBuilder->create('App\Forms\EndResultForm', [
      'method' => 'PATCH',
      'url'    => route('admin.results.update', $endResult),
      'model'  => $endResult,
    ]);

    // gender field
    $gender_tag = $endResult->tags()->where('tag', 'like', 'pg%')->first();

    $gender = '';
    if($gender_tag) {
      $gender = $gender_tag->tag;
    }

    $form->modify('gender', 'select', [
      'selected' => str_replace('pg', '', $gender),
    ]);

    // age field
    $age_tag = $endResult->tags()->where('tag', 'like', 'pa%')->first();

    $age = '';
    if($age_tag) {
      $age = $age_tag->tag;
    }

    $form->modify('age', 'select', [
      'selected' => str_replace('pa', '', $age),
    ]);

    // tags field
    $states_tag = $endResult->tags()->where('tag', 'like', 'st%')->pluck('tag');

    $states = [];
    if($states_tag) {
      foreach ($states_tag as $state_tag) {
        $states[] = str_replace('st', '', $state_tag);
      }
    }

    $form->modify('tags', 'select', [
      'selected' => $states,
    ]);

    $form->modify('submit', 'submit', [
      'label' => 'ویرایش',
    ]);

    return view('admin.end_results.create', compact('form'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param \App\EndResult            $endResult
   *
   * @return \Illuminate\Http\Response
   * @internal param \App\Question $question
   *
   * @internal param int $id
   */
  public function update(Request $request, EndResult $endResult) {
    // pg personal gender'
    if($request->get('gender')) {
      $gender = 'pg' . $request->get('gender');
    }

    if($request->get('age')) {
      // pa personal age
      $age = 'pa' . $request->get('age');
    }

    $states = (array) $request->get('tags');

    $new_states = [];
    foreach ($states as $index => $state) {
      $new_states[] = [
       'end_result_id' => $endResult->id,
       // st state
       'tag'           => 'st' . $state,
       'created_at'    => Carbon::now(),
       'updated_at'    => Carbon::now(),
      ];
    }

    $age_gender = [];
    $time = Carbon::now();

    if(isset($gender))
      $age_gender[] = [
       'end_result_id' => $endResult->id,
       'tag'           => $gender,
       'created_at'    => $time,
       'updated_at'    => $time,
      ];
    if(isset($age))
      $age_gender[] = [
       'end_result_id' => $endResult->id,
       'tag'           => $age,
       'created_at'    => $time,
       'updated_at'    => $time,
      ];

    $tag_end_result_values = array_merge($age_gender, $new_states);

    $parameter = compact('tag_end_result_values', 'endResult');
    DB::transaction(function () use ($parameter) {
      $endResult = $parameter['endResult'];
      $tag_end_result_values = $parameter['tag_end_result_values'];
      DB::table('tag_end_result')->where('end_result_id', '=', $endResult->id)->delete();
      TagEndResult::insert($tag_end_result_values);
    });

    return redirect()->back();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\EndResult $endResult
   *
   * @return \Illuminate\Http\Response
   * @internal param \App\Question $question
   *
   * @internal param int $id
   */
  public function destroy(EndResult $endResult) {

    // TODO:: this destroy method must change to Foreign key cascading delete. in tag_end_result, customer_end_result, related_products tables?
    DB::transaction(function () use ($endResult) {
      // tag_end_result -> end_result_id
      TagEndResult::destroy(TagEndResult::where('end_result_id', $endResult->id)
                                        ->pluck('id')
                                        ->toArray());
      // customer_end_result -> end_result_id
      CustomerEndResult::destroy(CustomerEndResult::where('end_result_id', $endResult->id)
                                                  ->pluck('id')
                                                  ->toArray());
      // related_products -> end_result_id
      RelatedProduct::destroy(RelatedProduct::where('end_result_id', $endResult->id)
                                            ->pluck('id')
                                            ->toArray());

      // delete end result
      EndResult::destroy($endResult->id);

    });

    return 1;
  }
}
