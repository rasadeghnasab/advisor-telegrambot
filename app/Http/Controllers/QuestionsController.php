<?php

namespace App\Http\Controllers;

use App\Answer;
use App\State;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Question;
use Kris\LaravelFormBuilder\FormBuilder as KFB;

class QuestionsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param State $state
   * @return \Illuminate\Http\Response
   */
  public function index(State $state)
  {
    // list all states
//    dd(Question::search('هپیکسf'));
    $questions = $state->questions()->weightOrder()->get();

    return view('admin.questions.list', compact('state', 'questions'));
  }

  public function weightOrder(Request $request)
  {
    $weights = $request->get('weight');
    foreach ($request->get('qid') as $index => $qid) {
      Question::where('id', $qid)->update(['weight' => $weights[$index]]);
    }

    return redirect()->back();
  }

  /**
   * Show the form for creating a new resource.
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   */
  public function create(KFB $formBuilder)
  {
    //
    $form = $formBuilder->create('App\Forms\QuestionForm', [
     'method' => 'POST',
     'url'    => route('admin.questions.store'),
    ]);

    return view('admin.questions.create', compact('form'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $question = Question::create(array_merge($request->all(), ['weight' => Question::all()->max('weight') + 1]));

    return redirect()->route('admin.questions.answers.index', $question);
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Question $question
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function show(Question $question)
  {
    return $question->state->name;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Question                        $question
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function edit(Question $question, KFB $formBuilder)
  {
    $form = $formBuilder->create('App\Forms\QuestionForm', [
     'method' => 'PATCH',
     'url'    => route('admin.questions.update', $question),
     'model'  => $question,
    ]);

    $form->modify('submit', 'submit', [
     'label' => 'ویرایش',
    ]);

    return view('admin.questions.create', compact('form'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param \App\Question             $question
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function update(Request $request, Question $question)
  {
    // if checkbox was not checked, we set 0 as default
    $request->merge(['save_answer' => $request->input('save_answer', 0), 'searchable' => $request->input('searchable', 0)]);
    $question->update($request->all());

    return redirect()->route('admin.questions.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Question $question
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function destroy(Question $question)
  {

    DB::beginTransaction();

    try {
      Answer::where('question_id', $question->id)->delete();
      $question->delete();

      DB::commit();

      return 'true';
    } catch (\Exception $e) {
      DB::rollback();

      // something went wrong
      return 'false';
    }
  }
}
