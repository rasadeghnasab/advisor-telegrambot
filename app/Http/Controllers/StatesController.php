<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStateRequest;
use App\State;
use App\StateGroupState;
use Collective\Html\FormBuilder;
use Illuminate\Http\Request;

use App\Http\Requests;
use Kris\LaravelFormBuilder\FormBuilder as KFB;

class StatesController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    // list all states
    $states = State::paginate(15);

    return view('admin.states.list', compact('states'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   */
  public function create(KFB $formBuilder, $state_groups = null) {
      //
      $form = $formBuilder->create('App\Forms\StateForm', [
        'method' => 'POST',
        'url'    => route('admin.state_groups.states.create', ['state_groups' => $state_groups->id]),
      ]);

      return view('admin.states.create', compact('form'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \App\Http\Requests\CreateStateRequest|\Illuminate\Http\Request $request
   *
   * @param null                                                           $state_groups
   * @return \Illuminate\Http\Response
   * @internal param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   */
  public function store(Request $request, $state_groups = null) {
    try {
      \DB::transaction(function () use ($request, $state_groups) {
        $state = State::create($request->all());
        if($state_groups) {
          StateGroupState::create([
           'state_group_id' => $state_groups->id,
           'state_id' => $state->id,
          ]);
        }
      });
    }
    catch (\Exception $e) {
      return redirect()->back()->withErrors('لطفا دوباره تلاش نمایید');
    }

    return redirect()->back();
  }

  /**
   * Display the specified resource.
   *
   * @param \App\State $state
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function show(State $state) {
    return redirect()->back();
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\State                                                        $state
   *
   * @param \Collective\Html\FormBuilder|\Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function edit(State $state, KFB $formBuilder) {
    $form = $formBuilder->create('App\Forms\StateForm', [
      'method' => 'PATCH',
      'url'    => route('admin.states.update', $state),
      'model'  => $state,
    ]);

    $form->modify('submit', 'submit', [
      'label' => 'ویرایش',
    ]);

    return view('admin.states.create', compact('form'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \App\Http\Requests\CreateStateRequest|\Illuminate\Http\Request $request
   * @param \App\State                                                     $state
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   */
  public function update(CreateStateRequest $request, State $state) {
    $request = array_merge($request->all(), ['deletable' => $request->has('deletable') ? 1 : 0]);
    $state->update($request);

    return redirect()->route('admin.states.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\State $state
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   *
   */
  public function destroy(State $state) {
    return State::destroy($state->id);
  }
}
