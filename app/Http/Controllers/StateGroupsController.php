<?php

namespace App\Http\Controllers;

use App\State;
use App\StateGroup;
use App\StateGroupState;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder as KFB;

use App\Http\Requests;

class StateGroupsController extends Controller {
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   * @internal param \App\StateGroup $state_groups
   *
   */
  public function index() {
    // list all state groups
    $state_groups = StateGroup::All();

    $states = State::whereNotIn('id', StateGroupState::all()->pluck('state_id')->toArray())->get();

    return view('admin.state_groups.list', compact('state_groups', 'states'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @param \Kris\LaravelFormBuilder\FormBuilder $formBuilder
   *
   * @return \Illuminate\Http\Response
   */
  public function create(KFB $formBuilder) {
    //
    $form = $formBuilder->create('App\Forms\StateGroupForm', [
      'method' => 'POST',
      'url'    => route('admin.state-groups.store'),
    ]);

    return view('admin.state_groups.create', compact('form'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    StateGroup::create($request->all());

    return redirect()->route('admin.state-groups.index');
  }

  /**
   * Display the specified resource.
   *
   * @param \App\StateGroup $state_group
   *
   * @return \Illuminate\Http\Response
   * @internal param \App\StateGroup $state_groups
   *
   * @internal param int $id
   */
  public function show(StateGroup $state_group) {
    $all_states = State::whereNotIn('id', $state_group->states->pluck('id'))->pluck('name', 'id');

    return view('admin.states.ungroup-list', ['states'      => $state_group->states,
                                      'state_group' => $state_group,
                                      'all_states' => $all_states,
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param KFB $formBuilder
   * @return \Illuminate\Http\Response
   * @internal param int $id
   *
   */
  public function edit(KFB $formBuilder) {
    //
    $form = $formBuilder->create('App\Forms\StateGroupForm', [
     'method' => 'POST',
     'url'    => route('admin.state-groups.store'),
    ]);

    return view('admin.state_groups.create', compact('form'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int                      $id
   *
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\StateGroup $state_groups
   *
   * @return \Illuminate\Http\Response
   * @internal param int $id
   *
   */
  public function destroy(StateGroup $state_groups) {
    StateGroupState::where('state_group_id', $state_groups->id)->delete();
    return StateGroup::destroy($state_groups->id);
  }

  public function stateStore(Request $request) {
    return StateGroupState::create($request->all());
  }
}
