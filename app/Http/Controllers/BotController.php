<?php

namespace App\Http\Controllers;

use App\BotApi;
use App\CurrentState;
use App\EndResult;
use App\PersonalInfo;
use App\Question;
use App\StateChange;
use Telegram\Bot\Api;
use Illuminate\Support\Facades\Log;

class BotController extends Controller
{

  private $telegram;
  private $updates;
  private $customer_id;
  private $message_text;

  public function __construct()
  {
    $this->telegram = new Api();
    $this->updates = $this->telegram->getWebhookUpdates();
//    $this->updates = $this->telegram->getUpdates();
//    Log::info($this->updates->getMessage()->getText());

  }

  /**
   * @return int
   * @internal param CurrentState $current_state
   *
   * @internal param \App\CurrentState $state
   */
  public function index()
  {
//    return $this->updates;
/*
    $this->telegram->sendMessage([
      'text' => 'دکتر بات در حال به روز رسانی می باشد.',
      'chat_id' => 126506089,
    ]);
    return 1;
//*/
//    return $this->updates;
    $user = $this->updates->getMessage()->getChat();
    $this->customer_id = $this->updates->getMessage()->getChat()->getId();
    $this->message_text = $this->updates->getMessage()->getText();
    $current_state = CurrentState::findCurrentState($this->customer_id);

    // Validate User input.
    if (! $current_state->isValidAnswer($this->message_text)) {
      if ($current_state->searchable() && $searchResult = Question::search($this->message_text)) {
        // if state is searchable and we have a result for this search.
        return (new BotApi())->sendSearchResult($current_state, $searchResult);
      }

      // if state is not searchable or we have no search result for that query.
      return (new BotApi())->sendIDoNotKnowMessage($current_state);
    }

    // Use personal information
    PersonalInfo::personalInfoManagement($user, $current_state, $this->message_text);

    // If this question answers must be saved, save the answer.
    $this->saveAnswerIfNeeded($current_state);

    $state_change = new StateChange($current_state, $current_state->router($this->router()));

    $action = $state_change->stateController($this->router());

    $result = (new BotApi($action))->sendMessage($state_change->new_state);

    CurrentState::updateAfterChangeState($result, $state_change);
  }

  public function test()
  {
    $results  = 1;
    return $results;
  }

  /**
   * Do some actions on webhook
   *
   * @param string $action
   *
   * @return string
   * @throws \Telegram\Bot\Exceptions\TelegramSDKException
   */
  public function webhook($action = 'set')
  {
    // bot name: drpersiantestbot
    $token = '113825445:AAGhEuJYzYZ7q-0uKIPZOkPCw3ENHY4lhhA';
    $this->telegram = new Api($token);
    $this->updates = $this->telegram->getWebhookUpdates();
    $result = 'no result';
    if ($action == 'set') {
      // set web hook for current bot (current bot determined from .env file)
      $response = $this->telegram->setWebhook([
          'url' => 'https://drpersian.club/telegram/76A6AF868DB16/first',
      ]);

      $result = 'set';
    }
    elseif ($action == 'rm') {
      // remove web hook
      $response = $this->telegram->removeWebhook();
      $result = 'rm';
    }

//    Log::info($response);

    return $result;
  }

  private function saveAnswerIfNeeded($current_state)
  {
    if ($current_state->id && $this->router() != 'back')
      $current_state->state->saveAnswer($this->message_text, $current_state->current_sub_state, $current_state->customer_id);
  }

  /**
   * Get last message which sent by the user and detect best response to send
   * back.
   *
   * @return array|void
   */
  private function router()
  {
    switch (strtolower($this->message_text)) {
      case 'back':
      case 'برگشت':
      case 'بازگشت':
        $result = 'back';
        break;
      case 'next':
        $result = 'next';
        break;
      case 'reset':
      case 'ریست':
        $result = '/reset';
        break;
      default:
        $result = $this->message_text;
    }

    return $result;
  }
}