<?php

namespace App\Http\Controllers;

use App\CustomerEndResult;
use App\PersonalInfo;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchController extends Controller
{
  public function index() {
    // This hole method is unusable.
    $result = PersonalInfo::all();

    $result = $result->map(function ($object) {
      $object->skin_type = 'hello';

      return $object;
    });

    return $result;
  }

  /**
   * Search for users who use this bot.
   *
   * @param string $query
   *
   * @return mixed
   */
  public function mainSearch($query = '') {
    return CustomerEndResult::endResultTags($query);
  }
}
