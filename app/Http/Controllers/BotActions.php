<?php

namespace App\Http\Controllers;

use App\Http\Requests\BroadCastMessageRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use JavaScript;
use Laracasts\Flash\Flash;
use Telegram\Bot\Api;

class BotActions extends Controller {
  public $telegram;
  public $updates;

  public function __construct() {
    $this->telegram = new Api();
    $this->updates  = $this->telegram->getWebhookUpdates();
  }

  public function broadCastMessage() {
    return view('admin.bot_actions.broadcast_message');
  }

  public function sendMessage(BroadCastMessageRequest $request) {
    $users = $request->get('users');

    $this->determineChatAction($request->file('file'), $users);

    if ($request->hasFile('file')) {
      $options = $this->uploadFile($request);
      $this->sendFile($users, ['caption' => str_limit($request->get('text'), 200)], $options);
      $this->rmFile($options['path']);
    }
    else {
      $params = [
        'text' => $request->get('text'),
      ];
      foreach($users as $user) {
        $params['chat_id'] = $user;
        $this->telegram->sendMessage($params);
      }
    }

    Flash::success('Message sent');

    return redirect()->back();
  }

  public function sendFile($users, $message = [], $options = []) {
    $mime    = $this->determineMimeType($options['mime']);
    $message[$mime['mime']] = $options['path'];
    $users = (array) $users;
    foreach ($users as $user) {
      $message['chat_id'] = $user;
      call_user_func([$this->telegram, $mime['method']], $message);
    }
  }

  private function uploadFile($request, $options = []) {
    $default_options = [
      'input_name'       => 'file',
      'destination_path' => 'uploads',
    ];

    $options          = array_merge($default_options, $options);
    $destination_path = 'uploads'; // upload path
    $extension        = $request->file($options['input_name'])
                                ->getClientOriginalExtension()
    ; // getting image extension
    $mime             = $request->file($options['input_name'])
                                ->getClientMimeType()
    ; // getting image mime type
    $file_name        = time() . 'r' . rand(11111, 99999) . '.' . $extension; // renaming image
    $request->file($options['input_name'])
            ->move($destination_path, $file_name)
    ; // uploading file to given path

    return [
      'file_name' => $file_name,
      'mime'      => $mime,
      'path'      => public_path() . "/{$destination_path}/{$file_name}",
    ];
  }

  private function determineMimeType($mime_type) {
    $mime_type = $this->mimeTypes($mime_type);
    switch ($mime_type) {
      case 'image':
        $mime_type = 'photo';
        break;
      case 'application':
        $mime_type = 'document';
        break;
    }

    return [
      'method' => camel_case('send_' . $mime_type),
      'mime'   => $mime_type,
    ];
  }

  private function mimeTypes($mime_type) {
    return explode('/', $mime_type)[0];
  }

  private function rmFile($file_address) {
    File::delete($file_address);
  }

  private function determineChatAction($file, $users) {
    $mime_type = $file ? $this->mimeTypes($file->getClientMimeType()) : 'text';
    $actions   = [
      'image' => 'upload_photo',
      'video' => 'record_video',
      'audio' => 'upload_audio',
      'application' => 'upload_document',
      'text' => 'typing',
    ];

    foreach ($users as $user) {
      $this->telegram->sendChatAction(['chat_id' => $user, 'action' => $actions[$mime_type]]);
    }
  }
}
