<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateStateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Does the user has permission to perform this sort of request
        // e.g. John can not edit a comment that was created by Jane
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
        ];
    }
}
