<?php

namespace App\Http\Middleware;

use App\PersonalInfo;
use Closure;
use Telegram\Bot\Api;
use App\Botan;
use Illuminate\Support\Facades\Log;

class BotanMiddleware {

  /**
   * @var string Botan_token
   */
  private $token;

  public function __construct()
  {
    $this->token = getenv('TELEGRAM_BOT_TOKEN');
  }


  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \Closure                 $next
   *
   * @return mixed
   */
  public function handle($request, Closure $next) {
    $telegram = new Api();
    $botan = new Botan($this->token);

    $message  = $telegram->getWebhookUpdates();

    $chat_id = $message->getMessage()->getChat()->getId();
    Log::info('chatId ' . $chat_id);

    $url = $this->_incomingMessage($message);

    // now send short_url to user instead of original_url, and get geography, OS, Device of user.
    $short_url = $botan->shortenUrl($url, $chat_id);

    // check to see user exists or not?
    if(!PersonalInfo::newUser($chat_id)->exists()) {
      $telegram->sendMessage([
        'chat_id' => $chat_id,
        'text' => $short_url,
      ]);

      PersonalInfo::create(['customer_id' => $chat_id]);
    }

    return $next($request);
  }

  public function _incomingMessage($message_json) {
    $messageObj  = json_decode($message_json, TRUE);
    $messageData = $messageObj['message'];

    $botan = new Botan($this->token);
    $url = $botan->track($messageData, $message_json->getMessage()->getText());

    return $url;
  }

}
