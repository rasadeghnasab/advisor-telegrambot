<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'telegram/76A6AF868DB16F'], function () {

  // Matches The "/telegram/first" URL
  Route::post('first', [
//    'middleware' => ['telegram_analysis'],
    'uses' => 'BotController@index'
  ]);
  Route::get('first', 'BotController@index');

  Route::get('webhook/{action}', [
    'middleware' => ['has_perm:_superadmin'],
    'uses'       => 'BotController@webhook',
  ]);
});
Route::get('test', 'BotController@test');
Route::group(['prefix' => 'telegram/76A6AF868DB16'], function () {

  // Matches The "/telegram/first" URL
  Route::post('first', [
//    'middleware' => ['telegram_analysis'],
'uses' => 'BotController@index'
  ]);
  Route::get('first', 'BotController@index');

  Route::get('webhook/{action}', [
   'middleware' => ['has_perm:_superadmin'],
   'uses'       => 'BotController@webhook',
  ]);
});

// super admin and bot-admin routes.
Route::group([
  'prefix'     => 'admin',
  'middleware' => ['has_perm:_superadmin,_bot-admin'],
], function () {
  Route::resource('states.questions', 'QuestionsController');
  Route::resource('states.questions.answers', 'AnswersController');
  Route::resource('results', 'EndResultController');
  Route::get('admin/results/{age}', 'EndResultController@resultsOnAge');
  Route::resource('results.products', 'RelatedProductsController');

  // ordering
  Route::post('questions/order', [
    'as'   => 'admin.states.questions.order',
    'uses' => 'QuestionsController@weightOrder',
  ]);
  Route::post('questions/answers/order', [
    'as'   => 'admin.questions.answers.order',
    'uses' => 'AnswersController@weightOrder',
  ]);
  Route::post('results/products/order', [
    'as'   => 'admin.results.products.order',
    'uses' => 'RelatedProductsController@weightOrder',
  ]);

  // tables for search and send message to users.
  Route::get('bot-actions/broadcast-message', ['as' => 'bot.action.index', 'uses' => 'BotActions@broadCastMessage']);
  Route::post('bot-actions/broadcast-message', ['as' => 'bot.action.sendMessage', 'uses' => 'BotActions@sendMessage']);
  // logs
  Route::get('bot-logs/', ['as' => 'bot.action.sendMessage', 'uses' => 'BotActions@sendMessage']);
});

// super admin routes.
Route::group([
  'prefix'     => 'admin',
  'middleware' => ['has_perm:_superadmin'],
], function () {
  Route::resource('states', 'StatesController');
  Route::resource('state-groups', 'StateGroupsController');
  Route::post('state-groups/add/state', 'StateGroupsController@stateStore');
  Route::get('state-groups/{state_groups}/state/create', 'StatesController@create')->name('admin.state_groups.states.create');;
  Route::post('state-groups/{state_groups}/state/create', 'StatesController@store')->name('admin.state_groups.states.store');;
  /*
  |--------------------------------------------------------------------------
  | Application Logs
  |--------------------------------------------------------------------------
  */
  Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::group([
  'prefix'     => 'api/v1',
  'middleware' => ['has_perm:_superadmin,_bot-admin'],
], function () {
  // Angular search
  Route::post('search/main-search/{query?}', 'SearchController@mainSearch');
  Route::post('search/index', 'SearchController@index');
//  Route::post('search/index', 'SearchController@index');
});

Route::get('admin/test', function(){
  $customer_id = 2;
  $currentState = \App\CurrentState::findCurrentState(65817282);

  return $currentState;
  $answeredQuestions = \App\AnsweredQuestion::where('customer_id', 126506089)->where('closed', 0)->pluck('answer_id');

  $answers = \App\Answer::whereIn('id', $answeredQuestions)->get();

  $result = 0;
  foreach ($answers as $answer) {
    dump($answer->id);
    dump($answer->getMeta('point'));
//        Log::info($answer);
    $result += $answer->getMeta('point');
  }
  dd($result);
});