$(document).ready(function(){
    var onDrop_fn = function(evt) {
        console.log(evt)
        var item = $(evt.item);
        $.each(item.parent('tbody').find('tr'), function(index){
            //$(this).find('.weight').val(index);
            //console.log('index', index);
            //console.log();
            $(this).find('.weight').val(index);
            console.log('index', index)
        });
    }
    var group = $('table.sortable tbody').sortable({
        group: 'simple_with_animation',
        filter: '.empty, .collection-header',
        onEnd: onDrop_fn
    });

    $(".select-select2").select2({
        dir: "rtl"
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.item.delete').on('click', function(e){
        e.preventDefault();
        var th = $(this);
        if(!confirm('از حذف اطمینان دارید؟')) {
            return false;
        }
        $.ajax({
            url: $(this).attr('href'),
            //url: 'test',
            method: 'DELETE',
        })
            .done(function(result){
                // Do something with the result
                if(result) {
                    th.parents('.deletable').hide(1500);
                }
            })
            .complete(function(result){
                console.log('complete', result)
            })
        ;
    });

         $('textarea').ckeditor({
             language: 'fa',
             uiColor: '#9AB8F3'
         });
});