function loadTableContentController($scope, $http) {
    /**
     * this function initialize this controller. :)
     */
    $scope.init = function () {
        $scope.query = '';
        $scope.change();
    };
    //$http.post('/api/v1/search/index')
    //    .success(function (rows) {
    //        $scope.contents = rows;
    //    });

    $scope.change = function () {
        $http.post('/api/v1/search/main-search/' + $scope.query)
            .success(function (rows) {
                console.log(rows);
                $scope.contents = rows;
            });
    }

    $scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.contents, function (content) {
            content.Selected = $scope.selectedAll;
        });

    };

    $scope.init();
}

function StateGroupController($scope, $http, $window) {
    $scope.state_group_add = function() {
        $http({
            url: '/admin/state-groups/add/state',
            method: "POST",
            data: { 'state_id' : $scope.stateId, 'state_group_id' : $scope.stateGroupId }
        })
            .success(function () {
                $window.location.reload();
            });
    }

    $scope.state_group_remove = function () {

    }
}