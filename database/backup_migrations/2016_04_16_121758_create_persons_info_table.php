<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->boolean('gender')->nullable();
            $table->tinyInteger('age')->nullable();
            $table->string('province', 127)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('person_info');
    }
}
