<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCloseColumnToCustomerEndResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_end_results', function (Blueprint $table) {
            $table->tinyInteger('closed')->after('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_end_results', function (Blueprint $table) {
            $table->dropColumn('closed');
        });
    }
}
