@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
            <div class="page-header">
                <h1>
                    {{--<i class="md md-add-circle-outline"></i>--}}
                    <a href="{{route('admin.state-groups.create')}}" class="md md-add-circle-outline text-primary">افزودن گروه</a>
                </h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        {!! Form::open(['route' => 'admin.states.questions.order']) !!}
                        <table class="table table-full table-full-small ">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($state_groups as $state_group)
                                <tr class="deletable">
                                    <td>{{ $state_group->name }}</td>
                                    <td>
                                        <a href="{{ route('admin.state-groups.edit', $state_group) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.state-groups.destroy', $state_group) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.state-groups.show', $state_group) }}"
                                           title="لیست حالت ها" class="btn btn-lg btn-round btn-info">
                                            <span class="md md-list"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        {!! Form::input('text', 'weight[]', $state_group->weight, ['class' => 'hidden weight']) !!}
                                        {!! Form::input('text', 'qid[]', $state_group->id, ['class' => 'hidden']) !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <section class="tables-data">
                <div class="page-header">
                    h2
                </div>
                <div class="card">
                    <div>
                        <div class="">
                            <table class="table table-full table-full-small" ng-controller="StateGroupController">
                                <thead>
                                <tr>
                                    <th>نام</th>
                                    <th>قابل جستجو</th>
                                    <th>رویداد در حین شروع مرحله</th>
                                    <th>تنظیمات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($states as $state)
                                    <tr class="deletable">
                                        <td>{{ $state->name }}</td>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" disabled {{$state->deletable ? 'checked' : ''}}>
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            {{ $state->result_type }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.states.edit', $state) }}"
                                               class="btn btn-lg btn-round btn-success">
                                                <span class="md md-mode-edit"></span>

                                                <div class="ripple-wrapper"></div>
                                            </a>
                                            <a href="{{ route('admin.states.destroy', $state) }}"
                                               class="btn btn-lg btn-round btn-success item delete">
                                                <span class="md md-delete"></span>

                                                <div class="ripple-wrapper"></div>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <div class="footer-buttons">
            <a href="{{route('admin.state-groups.create')}}"
               class="btn btn-primary btn-round btn-lg" data-title="گروه جدید"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
            {{--{!! Form::submit('Save', ['class' => 'btn btn-info btn-round btn-lg']) !!}--}}
        </div>
        {!! Form::close() !!}
    </div>
@endsection