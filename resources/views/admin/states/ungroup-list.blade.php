@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
            <div class="page-header">
                <h1>لیست حالت های {{ $state_group->name }}</h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        <table class="table table-full table-full-small" ng-controller="StateGroupController">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>قابل جستجو</th>
                                <th>رویداد در حین شروع مرحله</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($states as $state)
                                <tr class="deletable">
                                    <td>{{ $state->name }}</td>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" disabled {{$state->deletable ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $state->result_type }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.states.edit', $state) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.states.destroy', $state) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>نام حالت</td>
                                <td colspan="2">
                                <span>
                                    {!! Form::select('size', $all_states, NULL, ['ng-model' => 'stateId', 'class' => 'form-control']) !!}
                                    <input type="hidden" ng-model="stateGroupId"
                                           ng-init="stateGroupId = {{ $state_group->id }}">
                                </span>
                                </td>
                                <td>
                                    <button
                                            class="btn btn-success" ng-click="state_group_add()">
                                        <span class="md md-add"></span>
                                        <span>افزودن</span>

                                        <div class="ripple-wrapper"></div>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            <a href="{{route('admin.state_groups.states.create', ['state_groups' => $state_group->id])}}"
               class="btn btn-primary btn-round btn-lg" data-title="حالت جدید"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
            <a href="{{ route('admin.state-groups.index') }}" class="btn btn-lg btn-round btn-success pull-left">
                <span class="md md-keyboard-arrow-left"></span>
            </a>
        </div>
    </div>
@endsection