@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
            <div class="card">
                <div>
                    <div class="">
                        <table class="table table-full table-full-small" ng-controller="StateGroupController">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>قابل جستجو</th>
                                <th>رویداد در حین شروع مرحله</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($states as $state)
                                <tr class="deletable">
                                    <td>{{ $state->name }}</td>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" disabled {{$state->deletable ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $state->result_type }}
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.states.edit', $state) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.states.destroy', $state) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.states.questions.index', $state) }}"
                                           class="btn btn-lg btn-round btn-info">
                                            <span class="md md-list"></span>
                                            <div class="ripple-wrapper"></div>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            <a href="{{route('admin.states.create')}}"
               class="btn btn-primary btn-round btn-lg" data-title="حالت جدید"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
        </div>
    </div>
        {{ $states->links() }}
@endsection