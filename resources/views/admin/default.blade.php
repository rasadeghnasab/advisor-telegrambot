<!DOCTYPE html>
<html lang="en" ng-app>

<!-- Mirrored from www.theme-guys.com/materialism/html/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Apr 2016 10:54:09 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Materialism Angular Admin Theme">
    <meta name="author" content="Theme Guys - The Netherlands">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/favicon.ico">
    <title>Dashboard - Materialism</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>  <![endif]-->
    <link href="/assets/css/materialism/vendors.min.css" rel="stylesheet"/>
    <link href="/assets/css/materialism/styles.min.css" rel="stylesheet"/>
    <link href="/assets/css/materialism/material-rtl.css" rel="stylesheet"/>
    <link href="/assets/css/materialism/custom.css" rel="stylesheet"/>
    <link href="/assets/css/style.css" rel="stylesheet"/>
    <script charset="utf-8" src="https://maps.google.com/maps/api/js?sensor=true"></script>
</head>
<body scroll-spy="" id="top" class="theme-template-dark alert-open alert-with-mat-grow-top-left">

<main>
    @include('admin.layouts.sidebar')
    <div class="main-container">
        @include('errors.messages')
        @yield('content')
    </div>
</main>
<style>
    .glyphicon-spin-jcs {
        -webkit-animation: spin 1000ms infinite linear;
        animation: spin 1000ms infinite linear;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(359deg);
            transform: rotate(359deg);
        }
    }

    @keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(359deg);
            transform: rotate(359deg);
        }
    }
</style>
<!-- Start binding javascript variable to php -->
@include('admin.layouts.js_bind_variables')
<script src="/assets/js/materialism/vendors.min.js"></script>
<script src="/assets/js/materialism/app.min.js"></script>
<script src="/assets/js/jquery.fn.sortable.min.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script src="/assets/js/script.js"></script>
<script src="/assets/js/app/angular.1.2.5.min.js"></script>
<script src="/assets/js/app/app.js"></script>
</body>

<!-- Mirrored from www.theme-guys.com/materialism/html/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Apr 2016 10:54:42 GMT -->
</html>