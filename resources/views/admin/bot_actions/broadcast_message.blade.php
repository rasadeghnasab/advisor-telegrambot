@extends('admin.default')
@section('content')
    {!! Form::open(['url' => 'admin/bot-actions/broadcast-message', 'files' => true]) !!}
    <div id="search-container" ng-controller="loadTableContentController">
        <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
            <section class="tables-data">
                <div class="page-header">
                    <h1>ارسال پیام گروهی</h1>
                </div>
                <div class="card">
                    <div class="well grey darken-3">
                        <h4 class="white-text text-center">فرم ارسال اطلاعات به کاربران</h4>
                        <div class="form-group filled">
                            <label for="textArea" class="control-label">پیغام</label>
                            <textarea name='text' class="form-control vertical white-text" rows="3" id="textArea"></textarea>
                        </div>
                        <div class="form-group">
                        <span class="btn btn-primary fileinput-button">
                            <span>افزودن فایل</span>
                          <input type="file" name="file" class="">
                            <div class="ripple-wrapper"></div>
                        </span>
                        </div>
                        <div class="center-btn">

                            <button class="btn waves-effect waves-light btn-danger" type="submit" name="action">ارسال
                                <i class="md md-send left"></i>
                            </button>
                        </div>
                    </div>

                    <table class="table table-full table-full-small">
                        <thead>
                        <td>Name</td>
                        <td>Age</td>
                        <td>Gender</td>
                        <td>Province</td>
                        <td>Skin Type</td>
                        <td>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="left" ng-model="selectedAll" ng-click="checkAll()" >
                                </label>
                            </div>
                        </td>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="search" ng-model="query"
                                           ng-change="change()">
                                </div>
                            </td>
                        </tr>
                        <tr ng-repeat="content in contents | filter:query">
                            <td>@{{ content.firstname  }} @{{ content.lastname  }}</td>
                            <td>@{{ content.age }} </td>
                            <td>@{{ content.gender }} </td>
                            <td>@{{ content.province }} </td>
                            <td>@{{ content.skin_type }} </td>
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" ng-model="content.Selected" name='users[]'
                                               value="@{{ content.customer_id }}">
                                    </label>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </section>
        </div>
    </div>
    {!! Form::close() !!}
@endsection