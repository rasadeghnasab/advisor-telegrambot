@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
            <div class="page-header">
                <h1>
                    {{--<i class="md md-add-circle-outline"></i>--}}
                    <a href="{{route('admin.results.create')}}" class="md md-add-circle-outline text-primary">Add
                        Diagnostic</a>
                </h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        <table class="table table-full table-full-small">
                            <thead>
                            <tr>
                                <th>پیغام به کاربر</th>
                                <th>ویژگی ها</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($results as $index => $result)
                                <?php $tags = [] ?>
                                <tr class="deletable">
                                    <td>{{ $result->body }} </td>
                                    @foreach($result->tags as $tag)
                                        <?php $bare_tag = str_replace(['pg', 'pa', 'st'], '', $tag->tag) ?>
                                        @if(starts_with($tag->tag, 'pg'))
                                            <?php $tags[] = $bare_tag ? 'مرد' : 'زن'  ?>
                                        @elseif(starts_with($tag->tag, 'pa'))
                                            <?php $tags[] = App\Answer::find($bare_tag)->body ?>
                                        @else
                                            <?php $tags[] = App\State::find($bare_tag)['name'] ?>
                                        @endif
                                    @endforeach
                                    <td>{{ implode(', ', $tags) }}</td>
                                    <td>
                                        <a href="{{ route('admin.results.edit', $result) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.results.destroy', $result) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.results.products.index', $result) }}"
                                           title="لیست محصولات مرتبط" class="btn btn-lg btn-round btn-info">
                                            <span class="md md-list"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            <a href="{{route('admin.results.create')}}"
               class="btn btn-primary btn-round btn-lg" data-title="تشخیص"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
        </div>
    </div>
@endsection