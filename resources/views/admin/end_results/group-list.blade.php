@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
            <div class="page-header">
                <h1>Diagnostic Order by Age</h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        <table class="table table-full table-full-small">
                            <thead>
                            <tr>
                                <th>سن</th>
                                <th>مشاهده تشخیص ها</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($answers as $answer)
                                <tr class="deletable">
                                    <td>
                                        {{ $answer->body }}
                                    </td>
                                    <td>
                                        <a href="{{ action('EndResultController@resultsOnAge', $answer->body) }}"
                                           title="لیست تشخیص ها" class="btn btn-lg btn-round btn-info">
                                            <span class="md md-list"></span>
                                            <div class="ripple-wrapper"></div>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            <a href="{{route('admin.results.create')}}"
               class="btn btn-primary btn-round btn-lg" data-title="تشخیص"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
        </div>
    </div>
@endsection