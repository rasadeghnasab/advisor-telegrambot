@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
            <div class="page-header">
                <h1>
                    {{--<i class="md md-add-circle-outline"></i>--}}
                    <a href="{{route('admin.states.questions.create', ['states' => $state])}}" class="md md-add-circle-outline text-primary"> افزودن سوال</a>
                </h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        {!! Form::open(['route' => 'admin.states.questions.order', $state]) !!}
                        <table class="table table-full table-full-small sortable">
                            <thead>
                            <tr>
                                <th>نام</th>
                                <th>وابسته به</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($questions as $question)
                                <tr class="deletable">
                                    <td>{{ strip_tags(str_limit($question->body, 70)) }}</td>
                                    <td>{{ $question->state['name'] }}</td>
                                    <td>
                                        <a href="{{ route('admin.states.questions.edit', ['states' => $state, 'questions' => $question]) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.states.questions.destroy', ['states' => $state, 'questions' => $question]) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.states.questions.answers.index', ['states' => $state, 'questions' => $question]) }}"
                                           title="لیست سوالات" class="btn btn-lg btn-round btn-info">
                                            <span class="md md-list"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        {!! Form::input('text', 'weight[]', $question->weight, ['class' => 'hidden weight']) !!}
                                        {!! Form::input('text', 'qid[]', $question->id, ['class' => 'hidden']) !!}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            <a href="{{route('admin.states.questions.create', ['states' => $state])}}"
               class="btn btn-primary btn-round btn-lg" data-title="سوال جدید"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
            {!! Form::submit('Save', ['class' => 'btn btn-info btn-round btn-lg']) !!}
        </div>
        {!! Form::close() !!}
    </div>
{{--    {{ $questions->links() }}--}}
@endsection