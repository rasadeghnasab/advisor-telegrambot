<aside class="sidebar fixed" style="width: 260px; right: 0px; ">
    <div class="brand-logo">
        <div id="logo">
            <div class="foot1"></div>
            <div class="foot2"></div>
            <div class="foot3"></div>
            <div class="foot4"></div>
            <span class="logo_ay">Ay</span>
        </div>
        Persian
    </div>
    <div class="user-logged-in">
        <div class="content">
            <div class="user-name">{!! isset($user) ? $user->user_profile()->first()->first_name : 'User' !!}<span
                        class="text-muted f9"></span></div>
            <div class="user-email">{!! isset($user) ? $user->email : 'Email@email.com' !!}</div>
            <div class="user-actions">
                <a href="{!! URL::route('users.selfprofile.edit') !!}">Your profile</a>
                <a href="{!! URL::route('user.logout') !!}"> | Logout</a>
            </div>
        </div>
    </div>
    <ul class="menu-links">
        @if(isset($sidebar_items) && $sidebar_items)
            @foreach($sidebar_items as $name => $data)
                <li class="{!! LaravelAcl\Library\Views\Helper::get_active($data['url']) !!}">
                    <a href="{!! $data['url'] !!}">
                        {!! $data['icon'] !!}&nbsp;<span>{{$name}}</span></a>
                </li>
            @endforeach
        @endif
    </ul>
</aside>