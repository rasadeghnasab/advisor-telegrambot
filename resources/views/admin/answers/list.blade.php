@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="">
            <div class="page-header">
                <h1>{{ $question->body }}</h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        {!! Form::open(['route' => ['admin.questions.answers.order', $question]]) !!}

                        <table class="table table-full table-full-small sortable">
                            <thead>
                            <tr>
                                <th>متن پاسخ</th>
                                <th>مرحله بعدی</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($answers as $answer)
                                <tr class="deletable">
                                    <td>{{ $answer->body }}</td>
                                    <td>{{ $answer->next_state['name'] }}</td>
                                    <td>
                                        <a href="{{ route('admin.questions.answers.edit', [$question, $answer]) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.questions.answers.destroy', [$question, $answer]) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>
                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        {!! Form::input('text', 'weight[]', $answer->weight, ['class' => 'hidden weight']) !!}
                                        {!! Form::input('text', 'aid[]', $answer->id, ['class' => 'hidden']) !!}                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            {{--<div class="btn btn-default btn-round btn-lg m-r-10 btn-footer" scroll-to="top"
                 data-title="Scroll to top" data-toggle="tooltip" data-original-title="" title=""><i
                        class="md md-arrow-drop-up"></i>
                <div class="ripple-wrapper"></div>
            </div>--}}
            <a href="{{route('admin.questions.answers.create', $question)}}"
               class="btn btn-primary btn-round btn-lg" data-title="پاسخ جدید"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
            {!! Form::submit('Save', ['class' => 'btn btn-info btn-round btn-lg']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@endsection