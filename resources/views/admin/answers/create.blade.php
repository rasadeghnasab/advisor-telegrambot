@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <div class="col-md-10 col-md-push-1">
            <div class="well white">
                {!! form($form) !!}
            </div>
            <a href="{{ route('admin.questions.answers.index', $question) }}" class="btn btn-lg btn-round btn-primary pull-left">
                <span class="md md-keyboard-arrow-left"></span>
            </a>
        </div>
    </div>
@endsection
