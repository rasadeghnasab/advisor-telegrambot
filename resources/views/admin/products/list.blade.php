@extends('admin.default')

@section('content')
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="">
            <div class="page-header">
                <h1>{{ str_limit($result->body, 50) }}</h1>
            </div>
            <div class="card">
                <div>
                    <div class="">
                        {!! Form::open(['route' => ['admin.results.products.order', $result]]) !!}

                        <table class="table table-full table-full-small sortable">
                            <thead>
                            <tr>
                                <th>متن پاسخ</th>
                                <th>مرحله بعدی</th>
                                <th>تنظیمات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result->products as $product)
                                <tr class="deletable">
                                    <td>{{ $product->description }}</td>
                                    <td>
                                        <a href="{{ route('admin.results.products.edit', [$result, $product]) }}"
                                           class="btn btn-lg btn-round btn-success">
                                            <span class="md md-mode-edit"></span>

                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        <a href="{{ route('admin.results.products.destroy', [$result, $product]) }}"
                                           class="btn btn-lg btn-round btn-success item delete">
                                            <span class="md md-delete"></span>
                                            <div class="ripple-wrapper"></div>
                                        </a>
                                        {!! Form::input('text', 'weight[]', $product->weight, ['class' => 'hidden weight']) !!}
                                        {!! Form::input('text', 'aid[]', $product->id, ['class' => 'hidden']) !!}                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="footer-buttons">
            {{--<div class="btn btn-default btn-round btn-lg m-r-10 btn-footer" scroll-to="top"
                 data-title="Scroll to top" data-toggle="tooltip" data-original-title="" title=""><i
                        class="md md-arrow-drop-up"></i>
                <div class="ripple-wrapper"></div>
            </div>--}}
            <a href="{{route('admin.results.products.create', $result)}}"
               class="btn btn-primary btn-round btn-lg" data-title="افزودن محصول مرتبط"
               data-toggle="tooltip" data-original-title="" title="">
                <i class="md md-add"></i>
            </a>
            {!! Form::submit('Save', ['class' => 'btn btn-info btn-round btn-lg']) !!}
        </div>
        {!! Form::close() !!}

    </div>
@endsection