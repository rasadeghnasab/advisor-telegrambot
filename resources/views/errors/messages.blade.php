@if ($errors->any())
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <div class="col-md-10 col-md-push-1">
            <div class="bs-component">
                @foreach($errors->all() as $error)
                    <div class="alert alert-dismissible alert-warning">
                        <p>{{ $error }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif